<h1 align="left">ModioX</h1>
 
An open source desktop application designed to easily browse through a regularly updated database of game mods, homebrew, resources and themes for the PlayStation 3. A library that is populated by myself, few friends and awesome contributors, so that all mods have been tested and verified. It also utilizes the ftp commands to be able to install and uninstall modded files directly to games. Without the need for digging up old threads or using file managers - this aims to do everything for you. 

<h4 align="left">the only one of its kind (i think)</h4>

![Main Form](https://github.com/ohhsoash/ModioX/blob/master/.screenshots/demo/MainForm.png?raw=true) 

**Comments, ideas, suggestions?** Join the [Discord Server](https://discord.gg/FTCS3Xu) for support from our community!

## Features
* Fast, lightweight and simple to use
* Browse a large database of ps3 mods
* Complete with info, creator, version, etc.
* Filter by firmware, mod type and region
* Install and uninstall mods to games
* Automatically detect game region
* Backup and restore original game files
* File explorer with basic commands
* Save multiple console profiles

## Requirements
* An internet connection
* NET Framework 4.6.2
* PlayStation 3 with MFW
 
## Installation
Download and run the latest version of the Windows installer, "ModioX.Installer.Windows.exe" from the [releases](https://github.com/ohhsodead/ModioX/releases/latest) page.
 
## Contributing
You're also welcome to submit any pull requests with fixes and suggestions, like additional features for making this project even more great. Please open an issue so we can discuss things before going further, maybe we can work on this together!
 
## Requesting Mods
I know that not all mods aren't on our database yet. But it you open an issue with the details, they will be added for you.
 
## Credits / Libraries
- Appropriate Authors of all Mods
- [DarkUI for Winforms](https://github.com/RobinPerris/DarkUI)
- [Newtonsoft.Json](https://www.newtonsoft.com/json)
- [Apache log4net](https://logging.apache.org/log4net/)
 
## Disclaimer
I can accept no responsibility for any damage you cause to your system by using this tool. Follow the instructions so you shouldn't have any issues.

## License
This project is released under the GNU General Public License v3.
